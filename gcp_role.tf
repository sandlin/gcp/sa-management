/*
* This file is for creating GCP Roles
*/

/****************************************************************************************************************************************
 * Start Role dns-admin
 ****************************************************************************************************************************************/
# resource "google_project_iam_custom_role" "dns_admin" {
#   role_id     = "dns_admin"
#   title       = "DNS Admin"
#   description = "Allows service account to manage DNS."

#   permissions = [
#     "dns.changes.create",
#     "dns.changes.get",
#     "dns.changes.list",
#     "dns.changes.create",
#     "dns.managedZones.list",
#     "dns.managedZones.get",
#     "dns.managedZones.update",
#     "dns.managedZones.list",
#     "dns.resourceRecordSets.create",
#     "dns.resourceRecordSets.delete",
#     "dns.resourceRecordSets.list",
#     "dns.resourceRecordSets.update",
#   ]
# }
/****************************************************************************************************************************************
 * END Role dns-admin
 ****************************************************************************************************************************************/
