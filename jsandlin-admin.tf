resource "google_service_account" "gsa" {
  account_id   = var.adminsa.account_id
  display_name = var.adminsa.display_name
  description  = "Overall admin service account"
}

resource "google_project_iam_member" "admins" {
  project = var.project
  for_each = {
    for m in var.adminsa.roles : "${m}" => m
  }
  role   = each.value
  member = "serviceAccount:${google_service_account.gsa.email}"
}
