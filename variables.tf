variable "project" {
  description = "The GCP Project"
  default     = ""
}

variable "region" {
  description = "Region this goes to"
  default     = ""
}

variable "zone" {
  description = "Zone this goes to"
  default     = ""
}

variable "service_account_json" {
  description = "The value of the json creds file. Will be used by GitLab project vars to pass creds (until we have vault in place)"
  default     = ""
}
variable "service_account_file" {
  description = "The service account id to use; this value will only be used if service_account_json is not set."
  default     = ""
}

# https://discuss.hashicorp.com/t/having-difficulty-using-two-different-for-loops-in-the-same-resource/5010
variable "adminsa" {
  default = {
    account_id   = "adminsa"
    display_name = "adminsa"
    roles = {
      "policyAdmin"                = "roles/accesscontextmanager.policyAdmin"     # Full access to policies, access levels, and access zones
      "cloudkmsAdmin"              = "roles/cloudkms.admin"                       # Provides full access to Cloud KMS resources, except encrypt and decrypt operations. https://cloud.google.com/iam/docs/understanding-roles#cloud-kms-roles
      "cloudkmsCrypto"             = "roles/cloudkms.cryptoKeyEncrypterDecrypter" # Provides ability to use Cloud KMS resources for encrypt and decrypt operations only.
      "computeAdmin"               = "roles/compute.admin"                        # I don't like this; too open for privs. Can we trim?
      "computeInstanceAdmin"       = "roles/compute.instanceAdmin.v1"             # Permissions to create, modify, and delete virtual machine instances. This includes permissions to create, modify, and delete disks, and also to configure Shielded VM settings.
      "computeNetworkAdmin"        = "roles/compute.networkAdmin"                 # Permissions to create, modify, and delete networking resources, except for firewall rules and SSL certificates.
      "containerClusterAdmin"      = "roles/container.clusterAdmin"               # Required to manage cluster
      "serviceAccountAdmin"        = "roles/iam.serviceAccountAdmin"              # Required to create service account to manage cluster
      "serviceAccountUser"         = "roles/iam.serviceAccountUser"               # Why do I need admin and user?
      "roleAdmin"                  = "roles/iam.roleAdmin"                        # Provides access to all custom roles in the project.
      "serviceAccountTokenCreator" = "roles/iam.serviceAccountTokenCreator"       # Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
      "projectIamAdmin"            = "roles/resourcemanager.projectIamAdmin"      # DESCRIPTION: Provides permissions to administer IAM policies on projects.
    }

  }
}
