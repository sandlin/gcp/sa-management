# SA Management

![Project Stage][project-stage-shield]
![Maintenance][maintenance-shield]
![Pipeline Status][pipeline-shield]


[![License][license-shield]](LICENSE.md)
[![Maintaner][maintainer-shield]](https://gitlab.com/jsandlin)

## Table of Contents
[[_TOC_]]

## Overview
This repo is to manage service accounts. This way I can implement various capabilities but not use my root account.

This run will create roles & service accounts. It will a service account key then download the key in JSON format to a local file.

### Roles Created

### Service Accounts Created


## Output

## Usage
If running local, use the [install.sh](install.sh).

In order to utilize the pre-commit-hook, once you clone your repo, execute `pre-commit install`

####

<!-- Let's define some variables for ease of use. -->
[project-stage-shield]: https://img.shields.io/badge/project%20stage-development-yellowgreen.svg
[issue]: https://gitlab.com/sandlin/terraform/sa_management/issues
[license-shield]: https://img.shields.io/github/license/hassio-addons/repository-beta.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2021.svg
[maintainer-shield]: https://img.shields.io/badge/maintainer-James_Sandlin-blueviolet
[pipeline-shield]: https://gitlab.com/sandlin/terraform/sa_management/badges/groot/pipeline.svg
