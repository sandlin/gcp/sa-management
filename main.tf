
provider "google" {
  credentials = try(var.service_account_json != "" ? var.service_account_json : file(pathexpand(var.service_account_file)), 0)
  project     = var.project
}


# Store state in the project.
terraform {
  required_version = ">=1.0.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/30433120/terraform/state/groot"
    lock_address   = "https://gitlab.com/api/v4/projects/30433120/terraform/state/groot/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/30433120/terraform/state/groot/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}
